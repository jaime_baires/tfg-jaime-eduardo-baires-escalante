#+Reporte para los estudiantes de TFG
#+key: weekly-progress-report
#+group: reports
#+contributor: Domingo Gomez-Perez
#+default-filename: weekly-progress-report.org

#+TITLE: Progreso Semanal 
#+AUTHOR: Jaime Eduardo Baires Escalante
#+DATE: 14/12/2021

* Resumen
1) Se identificó en la herramienta UniTime, una serie de elementos básicos que hay que agregar para que se activen las opciones de agregar docentes y asignaturas:
- Academic session (periodo académico)
- Department
- Managers (se analizó que esto sería un requisito a expandir posteriormente-> repartir entre varias personas la gestión y consulta del calendario)
- Subject Areas
- Buildings
- Academic Areas
- Degrees

2) Se definió la próxima tarea a llevar a cabo, a fin de probar la herramienta:
Hacer un curso de prueba sencillo con algunas asignaturas, aulas y profesores. Probar agrupar asignaturas por un grado (Degree).
Revisar si los datos se pueden volcar directamente en la BD, sin necesidad de utilizar la interfaz (con el objetivo de realizar posteriormente una migración de datos desde un Excel).

3) Agregar a Diego el repositorio de BitBucket

4) Ver video que Domingo proporcionará.

** TODO [16%] Jaime-Eduardo Baires
- [X] Aprender a utilizar la plantilla de la memoria
- [ ] Aprender a utilizar Unitime
- [ ] Subir una primera versión de los horarios de Informática, Física y Matemáticas
- [ ] Modelar las posibles restricciones de los horarios
- [ ] Aprender a manejar los ficheros XML de los horarios
- [ ] Conseguir hacer que el exportador de los horarios de un formato parecido a lo que tiene la facultad de ciencias.



* Resultados y discusión de la semana pasada

Se instaló Gantt Project para la generación del diagrama de Gantt con los RF y RNF
Se instaló la BD demo en el entorno local
Lectura de PDF
Instalación de MagicDraw 18.0 Personal Edition para los diagramas
Comienzo de desarrollo de casos de uso (borrador PPT)


* Bibliografía consultada